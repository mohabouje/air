#!/bin/bash

DIR="$( cd "$( dirname "$0" )" && pwd )"

echo "Creating tmp folder"
mkdir tmp

echo "Updating the version of cmake"
cd ${DIR}/tmp
wget https://cmake.org/files/v3.11/cmake-3.11.1.tar.gz
tar -xzvf cmake-3.11.1.tar.gz
cd cmake-3.11.1
./bootstrap
make && make install

echo "Installing google test"
cd ${DIR}/tmp
git clone https://github.com/google/googletest.git
cd googletest
mkdir build && cd build
cmake ..
make && make install

echo "Installing Google Mock"
cd ${DIR}/tmp
git clone  https://github.com/google/googlemock.git
cd googlemock/googlemock
mkdir build && cd build
cmake ..
make && make install

echo "Installing Google Benchmark"
cd ${DIR}/tmp
git clone  https://github.com/google/benchmark.git
cd benchmark
mkdir build && cd build
cmake ..
make && make install

echo "Installing Spdlog"
cd ${DIR}/tmp
git clone  https://github.com/gabime/spdlog.git
cd spdlog
mkdir build && cd build
cmake ..
make && make install

echo "Clean all temporal files"
rm -rf ${DIR}/tmp
