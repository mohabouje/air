/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: version.hpp
 * Created at: 08/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_VERSION_HPP
#define OPENAIR_VERSION_HPP

// Configure the version information
#define A_VERSION_MAJOR 0
#define A_VERSION_MINOR 0
#define A_VERSION_MICRO 0
#define A_BUILD_DATE __DATE__
#define A_BUILD_TIME __TIME__

#endif // OPENAIR_VERSION_HPP
