/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: types.hpp
 * Created at: 08/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_TYPES_HPP
#define OPENAIR_TYPES_HPP

#include <cstdint>
#include <string>

namespace air {
    using boolean   = bool;
    using char_t    = char;
    using float32_t = float;
    using float64_t = double;
    using int8_t    = std::int8_t;
    using int16_t   = std::int16_t;
    using int32_t   = std::int32_t;
    using int64_t   = std::int64_t;
    using uint8_t   = std::uint8_t;
    using uint16_t  = std::uint16_t;
    using uint32_t  = std::uint32_t;
    using uint64_t  = std::uint64_t;
    using size_t    = std::size_t;
    using index_t   = std::size_t;

    using string = std::string;

    using real_t = air::float32_t;
} // namespace air

#endif // OPENAIR_TYPES_HPP
