/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: point.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_POINT_HPP
#define OPENAIR_POINT_HPP

#include "core/core.hpp"
#include <ostream>

namespace air {
    template <typename T>
    class A_EXPORT Point {
    public:
        using value_type      = T;
        using reference       = value_type&;
        using const_reference = const value_type&;

        A_EXPORT constexpr Point() noexcept                      = default;
        A_EXPORT constexpr Point(const Point<T>& other) noexcept = default;
        A_EXPORT constexpr Point(Point<T>&& other) noexcept      = default;
        A_EXPORT Point<T>& operator=(const Point<T>& other) noexcept = default;
        A_EXPORT Point<T>& operator=(Point<T>&& other) noexcept = default;
        A_EXPORT constexpr Point(value_type x, value_type y, value_type z) noexcept;

        A_EXPORT constexpr value_type x() const noexcept {
            return x;
        }
        A_EXPORT constexpr value_type y() const noexcept {
            return y;
        }
        A_EXPORT constexpr value_type z() const noexcept {
            return z;
        }

        A_EXPORT constexpr void set_x(value_type x) noexcept {
            x_ = x;
        }
        A_EXPORT constexpr void set_y(value_type y) noexcept {
            y_ = y;
        }
        A_EXPORT constexpr void set_z(value_type z) noexcept {
            z_ = z;
        }

        A_EXPORT constexpr reference rx() noexcept {
            return x;
        }
        A_EXPORT constexpr reference ry() noexcept {
            return y;
        }
        A_EXPORT constexpr reference rz() noexcept {
            return z;
        }

        A_EXPORT constexpr Point<T>& operator+=(Point<T> point) noexcept;
        A_EXPORT constexpr Point<T>& operator-=(Point<T> point) noexcept;
        A_EXPORT constexpr Point<T>& operator*=(Point<T> point) noexcept;
        A_EXPORT constexpr Point<T>& operator/=(Point<T> point) noexcept;

        A_EXPORT constexpr Point<T>& operator+=(value_type value) noexcept;
        A_EXPORT constexpr Point<T>& operator-=(value_type value) noexcept;
        A_EXPORT constexpr Point<T>& operator*=(value_type value) noexcept;
        A_EXPORT constexpr Point<T>& operator/=(value_type value) noexcept;

        A_EXPORT constexpr bool operator==(const Point<T>& rhs) const noexcept;
        A_EXPORT constexpr bool operator!=(const Point<T>& rhs) const noexcept;
        A_EXPORT constexpr bool operator<(const Point<T>& rhs) const noexcept;
        A_EXPORT constexpr bool operator>(const Point<T>& rhs) const noexcept;
        A_EXPORT constexpr bool operator<=(const Point<T>& rhs) const noexcept;
        A_EXPORT constexpr bool operator>=(const Point<T>& rhs) const noexcept;

        A_EXPORT friend std::ostream& operator<<(std::ostream& os, const Point<T>& point);

    private:
        value_type x_ = 0;
        value_type y_ = 0;
        value_type z_ = 0;
    };

    template <typename T>
    constexpr Point<T>::Point(Point<T>::value_type x, Point<T>::value_type y, Point<T>::value_type z) noexcept :
        x_(x),
        y_(y),
        z_(z) {}

    template <typename T>
    constexpr Point<T>& Point<T>::operator+=(const Point<T> point) noexcept {
        x_ += point.x_;
        y_ += point.y_;
        z_ += point.y_;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator-=(const Point<T> point) noexcept {
        x_ -= point.x_;
        y_ -= point.y_;
        z_ -= point.y_;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator*=(const Point<T> point) noexcept {
        x_ *= point.x_;
        y_ *= point.y_;
        z_ *= point.y_;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator/=(const Point<T> point) noexcept {
        x_ /= point.x_;
        y_ /= point.y_;
        z_ /= point.y_;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator+=(const Point<T>::value_type value) noexcept {
        x_ += value;
        y_ += value;
        z_ += value;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator-=(const Point<T>::value_type value) noexcept {
        x_ -= value;
        y_ -= value;
        z_ -= value;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator*=(const Point<T>::value_type value) noexcept {
        x_ *= value;
        y_ *= value;
        z_ *= value;
        return *this;
    }

    template <typename T>
    constexpr Point<T>& Point<T>::operator/=(const Point<T>::value_type value) noexcept {
        x_ /= value;
        y_ /= value;
        z_ /= value;
        return *this;
    }

    template <typename T>
    constexpr bool Point<T>::operator==(const Point<T>& rhs) const noexcept {
        return (x_ == rhs.x_) && (y_ == rhs.y_) && (z_ == rhs.z_);
    }

    template <typename T>
    constexpr bool Point<T>::operator!=(const Point<T>& rhs) const noexcept {
        return !(rhs == *this);
    }

    template <typename T>
    constexpr bool Point<T>::operator<(const Point<T>& rhs) const noexcept {
        if (x_ < rhs.x_)
            return true;
        if (rhs.x_ < x_)
            return false;
        if (y_ < rhs.y_)
            return true;
        if (rhs.y_ < y_)
            return false;
        return z_ < rhs.z_;
    }

    template <typename T>
    constexpr bool Point<T>::operator>(const Point<T>& rhs) const noexcept {
        return rhs < *this;
    }

    template <typename T>
    constexpr bool Point<T>::operator<=(const Point<T>& rhs) const noexcept {
        return !(rhs < *this);
    }

    template <typename T>
    constexpr bool Point<T>::operator>=(const Point<T>& rhs) const noexcept {
        return !(*this < rhs);
    }

    template <typename T>
    std::ostream& operator<<(std::ostream& os, const Point<T>& point) {
        os << "{ x: " << point.x_ << " y: " << point.y_ << " z: " << point.z_ << " }";
        return os;
    }

    using Point_ui8  = Point<air::uint8_t>;
    using Point_ui16 = Point<air::uint16_t>;
    using Point_ui32 = Point<air::uint32_t>;
    using Point_ui64 = Point<air::uint64_t>;

    using Point_i8  = Point<air::int8_t>;
    using Point_i16 = Point<air::int16_t>;
    using Point_i32 = Point<air::int32_t>;
    using Point_i64 = Point<air::int64_t>;

    using Point_f32 = Point<air::float32_t>;
    using Point_f64 = Point<air::float64_t>;
} // namespace air

#endif // OPENAIR_POINT_HPP
