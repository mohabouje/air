/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: Logger.hpp
 * Created at: 08/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_LOGGER_HPP
#define OPENAIR_LOGGER_HPP

#include "core/core.hpp"
#include <spdlog/spdlog.h>

namespace air {

    class A_EXPORT Logger {
    public:
        enum class MsgType { Info = 0, Debug, Warning, Critical, Error };

        A_EXPORT Logger(const std::string& type, MsgType msgType);
        A_EXPORT ~Logger();
        A_EXPORT Logger& operator<<(air::char_t);
        A_EXPORT Logger& operator<<(air::boolean);
        A_EXPORT Logger& operator<<(air::int8_t);
        A_EXPORT Logger& operator<<(air::int16_t);
        A_EXPORT Logger& operator<<(air::int32_t);
        A_EXPORT Logger& operator<<(air::int64_t);
        A_EXPORT Logger& operator<<(air::uint8_t);
        A_EXPORT Logger& operator<<(air::uint16_t);
        A_EXPORT Logger& operator<<(air::uint32_t);
        A_EXPORT Logger& operator<<(air::uint64_t);
        A_EXPORT Logger& operator<<(air::float32_t);
        A_EXPORT Logger& operator<<(air::float64_t);
        A_EXPORT Logger& operator<<(const air::char_t*);
        A_EXPORT Logger& operator<<(const air::string&);

    private:
        Logger& space();
        std::shared_ptr<spdlog::logger> logger_{nullptr};
        Logger::MsgType msgType_{MsgType::Info};
        std::string msg_{""};
    };
} // namespace air

#define aInfo() air::Logger("console", Logger::MsgType::Info)
#define aDebug() air::Logger("console", Logger::MsgType::Debug)
#define aWarning() air::Logger("console", Logger::MsgType::Warning)
#define aCritical() air::Logger("console", Logger::MsgType::Critical)
#define aError() air::Logger("console", Logger::MsgType::Error)

#endif // OPENAIR_LOGGER_HPP
