/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: dft.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_DFT_HPP
#define OPENAIR_DFT_HPP

#include "fftw_impl.hpp"

namespace air {
    namespace dsp {
        template <typename InputIterator, typename OutputIterator>
        inline void c_dft(InputIterator first, InputIterator last, OutputIterator out) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type,
                                       std::complex<air::real_t>>::value,
                          "Expected complex air::data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type,
                                       std::complex<air::real_t>>::value,
                          "Expected complex air::data");
            fftw_plan<air::real_t> plan;
            plan.dft(fftw_cast(&(*first)), fftw_cast(&(*out)), static_cast<air::size_t>(std::distance(first, last)));
        };

        template <typename Container>
        inline void c_dft(const Container& input, Container& output) {
            static_assert(std::is_same<typename Container::value_type, std::complex<air::real_t>>::value,
                          "Expected complex air::data");
            fftw_plan<air::real_t> plan;
            plan.dft(fftw_cast(air::data(input)), fftw_cast(air::data(output)), air::size(input));
        };

        template <typename InputIterator, typename OutputIterator>
        inline void c_idft(InputIterator first, InputIterator last, OutputIterator out) {
            using value_type = typename std::iterator_traits<InputIterator>::value_type;
            static_assert(std::is_same<value_type, std::complex<air::real_t>>::value, "Expected complex air::data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type,
                                       std::complex<air::real_t>>::value,
                          "Expected complex air::data");

            const auto nfft = static_cast<air::size_t>(std::distance(first, last));
            fftw_plan<air::real_t> plan;
            plan.idft(fftw_cast(&(*first)), fftw_cast(&(*out)), nfft);
            std::transform(out, out + nfft, out, [nfft](value_type value) -> value_type {
                return value / static_cast<value_type>(nfft);
                ;
            });
        };

        template <typename Container>
        inline void c_idft(const Container& input, Container& output) {
            using value_type = typename Container::value_type;
            static_assert(std::is_same<value_type, std::complex<air::real_t>>::value, "Expected complex air::data");
            fftw_plan<air::real_t> plan;
            plan.idft(fftw_cast(air::data(input)), fftw_cast(air::data(output)), air::size(input));
            std::transform(std::cbegin(output), std::cend(output), std::begin(output),
                           [nfft = air::size(output)](value_type value) -> value_type {
                               return value / static_cast<value_type>(nfft);
                           });
        };

        template <typename InputIterator, typename OutputIterator>
        void dft(InputIterator first, InputIterator last, OutputIterator out) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type, air::real_t>::value,
                          "Expected real_t air::data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type,
                                       std::complex<air::real_t>>::value,
                          "Expected complex real_t air::data");
            fftw_plan<air::real_t> plan;
            plan.dft(fftw_cast(&(*first)), fftw_cast(&(*out)), static_cast<air::size_t>(std::distance(first, last)));
        };

        template <typename R_Container, typename C_Container>
        void dft(const R_Container& input, C_Container& output) {
            static_assert(std::is_same<typename C_Container::value_type, std::complex<air::real_t>>::value,
                          "Expected complex real_t air::data");
            static_assert(std::is_same<typename R_Container::value_type, air::real_t>::value,
                          "Expected real_t air::data");
            fftw_plan<air::real_t> plan;
            plan.dft(fftw_cast(air::data(input)), fftw_cast(air::data(output)), air::size(input));
        };

        template <typename InputIterator, typename OutputIterator>
        void idft(InputIterator first, InputIterator last, OutputIterator out) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type,
                                       std::complex<air::real_t>>::value,
                          "Expected complex real_t air::data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type, air::real_t>::value,
                          "Expected real_t air::data");

            const auto nfft = static_cast<air::size_t>(std::distance(first, last));
            fftw_plan<air::real_t> plan;
            plan.idft(fftw_cast(&(*out)), fftw_cast(&(*first)), nfft);
            std::transform(out, out + nfft, out, [nfft](air::real_t value) { return value / nfft; });
        };

        template <typename R_Container, typename C_Container>
        void idft(const C_Container& input, R_Container& output) {
            static_assert(std::is_same<typename C_Container::value_type, std::complex<air::real_t>>::value,
                          "Expected complex real_t air::data");
            static_assert(std::is_same<typename R_Container::value_type, air::real_t>::value,
                          "Expected real_t air::data");
            fftw_plan<air::real_t> plan;
            plan.idft(fftw_cast(air::data(input)), fftw_cast(air::data(output)), air::size(input));
            std::transform(std::cbegin(output), std::cend(output), std::begin(output),
                           [nfft = air::size(output)](air::real_t value) { return value / nfft; });
        };

    } // namespace dsp

} // namespace air

#endif //OPENAIR_DFT_HPP
