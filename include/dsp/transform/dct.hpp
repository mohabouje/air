/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: dct.hpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_DCT_HPP
#define OPENAIR_DCT_HPP

#include "fftw_impl.hpp"
namespace air {
    namespace dsp {
        template <typename InputIterator, typename OutputIterator>
        inline void dct(InputIterator first, InputIterator last, OutputIterator out,
                        DCT_Type type = DCT_Type::Type_II) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type, air::real_t>::value,
                          "Expected real_t data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type, air::real_t>::value,
                          "Expected real_t data");
            fftw_plan<air::real_t> plan;
            plan.dct(fftw_cast(&(*first)), fftw_cast(&(*out)), static_cast<air::size_t>(std::distance(first, last)),
                     type);
        };

        template <typename Container>
        inline void dct(const Container& input, Container& output, DCT_Type type = DCT_Type::Type_II) {
            static_assert(std::is_same<typename Container::value_type, air::real_t>::value, "Expected real_t data");
            fftw_plan<air::real_t> plan;
            plan.dct(fftw_cast(air::data(input)), fftw_cast(air::data(output)), air::size(input), type);
        };

        template <typename InputIterator, typename OutputIterator>
        inline void idct(InputIterator first, InputIterator last, OutputIterator out,
                         DCT_Type type = DCT_Type::Type_II) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type, air::real_t>::value,
                          "Expected real_t data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type, air::real_t>::value,
                          "Expected real_t data");
            const auto nfft    = static_cast<air::size_t>(std::distance(first, last));
            const auto scaling = (type == DCT_Type::Type_I) ? 2 * (nfft - 1) : 2 * nfft;

            fftw_plan<air::real_t> plan;
            plan.idct(fftw_cast(&(*first)), fftw_cast(&(*out)), nfft, type);
            std::transform(out, out + nfft, out, [scaling](air::real_t value) { return value / scaling; });
        };

        template <typename Container>
        inline void idct(const Container& input, Container& output, DCT_Type type = DCT_Type::Type_II) {
            static_assert(std::is_same<typename Container::value_type, air::real_t>::value, "Expected real_t data");
            const auto nfft    = static_cast<air::size_t>(air::size(input));
            const auto scaling = (type == DCT_Type::Type_I) ? 2 * (nfft - 1) : 2 * nfft;

            fftw_plan<air::real_t> plan;
            plan.idct(fftw_cast(air::data(input)), fftw_cast(air::data(output)), nfft, type);
            std::transform(std::cbegin(output), std::cend(output), std::begin(output),
                           [scaling](air::real_t value) { return value / scaling; });
        };
    } // namespace dsp
} // namespace air

#endif //OPENAIR_DCT_HPP
