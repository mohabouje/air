/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: dft.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_FFTW_IMPL_HPP
#define OPENAIR_FFTW_IMPL_HPP

#include <fftw3.h>
#include <complex>
#include "core/core.hpp"
#include "meta/meta.hpp"

namespace air {
    namespace dsp {

        enum class DCT_Type { Type_I, Type_II, Type_III, Type_IV };

        template <typename T>
        inline T* fftw_cast(const T* p) {
            return const_cast<T*>(p);
        }

        inline fftwf_complex* fftw_cast(const std::complex<air::float32_t>* p) {
            return const_cast<fftwf_complex*>(reinterpret_cast<const fftwf_complex*>(p));
        }

        inline fftw_complex* fftw_cast(const std::complex<air::float64_t>* p) {
            return const_cast<fftw_complex*>(reinterpret_cast<const fftw_complex*>(p));
        }

        template <typename T>
        struct fftw_plan {};

        template <>
        struct fftw_plan<air::float32_t> {
            using scalar_type  = air::float32_t;
            using complex_type = fftwf_complex;
            using size_t       = air::size_t;

            ::fftwf_plan plan_{nullptr};
            fftw_plan() = default;
            ~fftw_plan() {
                if (notnull(plan_))
                    fftwf_destroy_plan(plan_);
            }

            inline void dft(complex_type* src, complex_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftwf_plan_dft_1d(nfft, src, dst, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftwf_execute_dft(plan_, src, dst);
            }

            inline void idft(complex_type* src, complex_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftwf_plan_dft_1d(nfft, src, dst, FFTW_BACKWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftwf_execute_dft(plan_, src, dst);
            }

            inline void dft(scalar_type* src, complex_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftwf_plan_dft_r2c_1d(nfft, src, dst, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftwf_execute_dft_r2c(plan_, src, dst);
            }

            inline void idft(complex_type* src, scalar_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftwf_plan_dft_c2r_1d(nfft, src, dst, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftwf_execute_dft_c2r(plan_, src, dst);
            }

            inline void dht(scalar_type* src, scalar_type* dst, size_t nfft) {
                if (notnull(plan_)) {
                    plan_ = fftwf_plan_r2r_1d(nfft, src, dst, FFTW_DHT, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                }
                fftwf_execute_r2r(plan_, src, dst);
            }

            inline void dct(scalar_type* src, scalar_type* dst, size_t nfft, DCT_Type type) {
                if (notnull(plan_)) {
                    const auto plan_type = [type]() {
                        switch (type) {
                            case DCT_Type::Type_I:
                                return FFTW_REDFT00;
                            case DCT_Type::Type_II:
                                return FFTW_REDFT10;
                            case DCT_Type::Type_III:
                                return FFTW_REDFT01;
                            case DCT_Type::Type_IV:
                                return FFTW_REDFT11;
                        }
                    }();
                    plan_ = fftwf_plan_r2r_1d(nfft, src, dst, plan_type, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                }
                fftwf_execute_r2r(plan_, src, dst);
            }

            inline void idct(scalar_type* src, scalar_type* dst, size_t nfft, DCT_Type type) {
                if (notnull(plan_)) {
                    const auto plan_type = [type]() {
                        switch (type) {
                            case DCT_Type::Type_I:
                                return FFTW_REDFT00;
                            case DCT_Type::Type_II:
                                return FFTW_REDFT01;
                            case DCT_Type::Type_III:
                                return FFTW_REDFT10;
                            case DCT_Type::Type_IV:
                                return FFTW_REDFT11;
                        }
                    }();
                    plan_ = fftwf_plan_r2r_1d(nfft, src, dst, plan_type, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                }
                fftwf_execute_r2r(plan_, src, dst);
            }
        };

        template <>
        struct fftw_plan<air::float64_t> {
            using scalar_type  = air::float64_t;
            using complex_type = fftw_complex;
            using size_t       = air::size_t;

            ::fftw_plan plan_{nullptr};
            fftw_plan() = default;
            ~fftw_plan() {
                if (notnull(plan_))
                    fftw_destroy_plan(plan_);
            }

            inline void dft(complex_type* src, complex_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftw_plan_dft_1d(nfft, src, dst, FFTW_FORWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftw_execute_dft(plan_, src, dst);
            }

            inline void idft(complex_type* src, complex_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftw_plan_dft_1d(nfft, src, dst, FFTW_BACKWARD, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftw_execute_dft(plan_, src, dst);
            }

            inline void dft(scalar_type* src, complex_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftw_plan_dft_r2c_1d(nfft, src, dst, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftw_execute_dft_r2c(plan_, src, dst);
            }

            inline void idft(complex_type* src, scalar_type* dst, size_t nfft) {
                if (notnull(plan_))
                    plan_ = fftw_plan_dft_c2r_1d(nfft, src, dst, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                fftw_execute_dft_c2r(plan_, src, dst);
            }

            inline void dht(scalar_type* src, scalar_type* dst, size_t nfft) {
                if (notnull(plan_)) {
                    plan_ = fftw_plan_r2r_1d(nfft, src, dst, FFTW_DHT, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                }
                fftw_execute_r2r(plan_, src, dst);
            }

            inline void dct(scalar_type* src, scalar_type* dst, size_t nfft, DCT_Type type) {
                if (notnull(plan_)) {
                    const auto plan_type = [type]() {
                        switch (type) {
                            case DCT_Type::Type_I:
                                return FFTW_REDFT00;
                            case DCT_Type::Type_II:
                                return FFTW_REDFT10;
                            case DCT_Type::Type_III:
                                return FFTW_REDFT01;
                            case DCT_Type::Type_IV:
                                return FFTW_REDFT11;
                        }
                    }();
                    plan_ = fftw_plan_r2r_1d(nfft, src, dst, plan_type, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                }
                fftw_execute_r2r(plan_, src, dst);
            }

            inline void idct(scalar_type* src, scalar_type* dst, size_t nfft, DCT_Type type) {
                if (notnull(plan_)) {
                    const auto plan_type = [type]() {
                        switch (type) {
                            case DCT_Type::Type_I:
                                return FFTW_REDFT00;
                            case DCT_Type::Type_II:
                                return FFTW_REDFT01;
                            case DCT_Type::Type_III:
                                return FFTW_REDFT10;
                            case DCT_Type::Type_IV:
                                return FFTW_REDFT11;
                        }
                    }();
                    plan_ = fftw_plan_r2r_1d(nfft, src, dst, plan_type, FFTW_ESTIMATE | FFTW_PRESERVE_INPUT);
                }
                fftw_execute_r2r(plan_, src, dst);
            }
        };

    } // namespace dsp
} // namespace air

#endif //OPENAIR_FFTW_IMPL_HPP
