/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: dht.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_DHT_HPP
#define OPENAIR_DHT_HPP

#include "fftw_impl.hpp"
namespace air {
    namespace dsp {
        template <typename InputIterator, typename OutputIterator>
        void dht(InputIterator first, InputIterator last, OutputIterator out) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type, air::real_t>::value,
                          "Expected real_t data");
            static_assert(std::is_same<typename std::iterator_traits<OutputIterator>::value_type, air::real_t>::value,
                          "Expected real_t data");
            fftw_plan<air::real_t> plan;
            plan.dht(fftw_cast(&(*first)), fftw_cast(&(*out)), static_cast<air::size_t>(std::distance(first, last)));
        };

        template <typename Container>
        void dht(const Container& input, Container& output) {
            static_assert(std::is_same<typename Container::value_type, air::real_t>::value, "Expected real_t data");
            fftw_plan<air::real_t> plan;
            plan.dht(fftw_cast(air::data(input)), fftw_cast(air::data(output)), air::size(input));
        };

    } // namespace dsp
} // namespace air

#endif //OPENAIR_DHT_HPP
