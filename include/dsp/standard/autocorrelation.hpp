/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: autocorrelation.hpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_AUTOCORRELATION_HPP
#define OPENAIR_AUTOCORRELATION_HPP

#include <vector>
#include <base/base.hpp>
#include "dsp/transform/fftw_impl.hpp"

namespace air {
    namespace dsp {

        class AutoCorrelation {
        public:
            enum class ScaleOpt { None, Biased };
            explicit AutoCorrelation(air::size_t sz, ScaleOpt opt = ScaleOpt::None);
            air::size_t size() const noexcept;

            template <typename InputIterator, typename OutputIterator>
            inline void compute(InputIterator first, InputIterator last, OutputIterator out);

        private:
            void compute(const real_t* input, real_t* output, air::size_t size);

        private:
            std::vector<std::complex<air::real_t>> fft_data_{};
            fftw_plan<air::real_t> fft_{};
            fftw_plan<air::real_t> ifft_{};
            air::size_t size_{0};
            ScaleOpt scale_{ScaleOpt::None};
        };

        template <typename InputIterator, typename OutputIterator>
        void AutoCorrelation::compute(InputIterator first, InputIterator last, OutputIterator out) {
            static_assert(std::is_same<typename std::iterator_traits<InputIterator>::value_type, real_t>::value &&
                              std::is_same<typename std::iterator_traits<OutputIterator>::value_type, real_t>::value,
                          "Iterator does not math the value type. No implicit conversion is allowed");
            const auto sz = std::distance(first, last);
            compute(&(*first), &(*out), sz);
        }
    } // namespace dsp
} // namespace air
#endif //OPENAIR_AUTOCORRELATION_HPP
