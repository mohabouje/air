/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: biquad.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_BIQUAD_HPP
#define OPENAIR_BIQUAD_HPP

#include "core/core.hpp"
#include <cmath>
#include <functional>
#include <algorithm>

namespace air {
    namespace dsp {
        inline namespace filter {
            class A_EXPORT Biquad {
            public:
                A_EXPORT constexpr Biquad(real_t a0, real_t a1, real_t a2, real_t b0, real_t b1, real_t b2) noexcept;
                A_EXPORT constexpr Biquad() noexcept              = default;
                A_EXPORT constexpr Biquad(const Biquad&) noexcept = default;
                A_EXPORT constexpr Biquad(Biquad&&) noexcept      = default;
                A_EXPORT constexpr Biquad& operator=(const Biquad&) noexcept = default;
                A_EXPORT constexpr Biquad& operator=(Biquad&&) noexcept = default;

                A_EXPORT constexpr real_t a0() const noexcept;
                A_EXPORT constexpr real_t a1() const noexcept;
                A_EXPORT constexpr real_t a2() const noexcept;
                A_EXPORT constexpr real_t b0() const noexcept;
                A_EXPORT constexpr real_t b1() const noexcept;
                A_EXPORT constexpr real_t b2() const noexcept;

                A_EXPORT constexpr void set_a0(real_t value) noexcept;
                A_EXPORT constexpr void set_a1(real_t value) noexcept;
                A_EXPORT constexpr void set_a2(real_t value) noexcept;
                A_EXPORT constexpr void set_b0(real_t value) noexcept;
                A_EXPORT constexpr void set_b1(real_t value) noexcept;
                A_EXPORT constexpr void set_b2(real_t value) noexcept;

                template <typename BiIterator>
                A_EXPORT constexpr void apply(BiIterator first, BiIterator last);

                template <typename InputIterator, typename OutputIterator>
                A_EXPORT constexpr void apply(InputIterator first, InputIterator last, OutputIterator out);

                A_EXPORT constexpr void reset() noexcept;
                A_EXPORT constexpr bool is_stable() const noexcept;

                A_EXPORT constexpr real_t operator()(real_t tick) noexcept;
                A_EXPORT constexpr operator bool() const noexcept;

            private:
                real_t b2_{0};
                real_t b1_{0};
                real_t b0_{1};
                real_t a2_{0};
                real_t a1_{0};
                real_t a0_{1};
                real_t w0_{0};
                real_t w1_{0};
            };

            constexpr Biquad::Biquad(real_t a0, real_t a1, real_t a2, real_t b0, real_t b1, real_t b2) noexcept :
                b2_(b2 / a0),
                b1_(b1 / a0),
                b0_(b0 / a0),
                a2_(a2 / a0),
                a1_(a1 / a0),
                a0_(1) {}

            constexpr void Biquad::reset() noexcept {
                w0_ = 0;
                w1_ = 1;
            }

            constexpr bool Biquad::is_stable() const noexcept {
                return std::abs(a2_) < 1 && (std::abs(a1_) < (1 + a2_));
            }

            constexpr void Biquad::set_a0(const real_t value) noexcept {
                a0_ = value;
                reset();
            }

            constexpr void Biquad::set_a1(const real_t value) noexcept {
                a1_ = value;
                reset();
            }

            constexpr void Biquad::set_a2(const real_t value) noexcept {
                a2_ = value;
                reset();
            }

            constexpr void Biquad::set_b0(const real_t value) noexcept {
                b0_ = value;
                reset();
            }

            constexpr void Biquad::set_b1(const real_t value) noexcept {
                b1_ = value;
                reset();
            }

            constexpr void Biquad::set_b2(const real_t value) noexcept {
                b2_ = value;
                reset();
            }

            template <typename BiIterator>
            constexpr void Biquad::apply(BiIterator first, BiIterator last) {
                apply(first, last, first);
            }

            template <typename InputIterator, typename OutputIterator>
            constexpr void Biquad::apply(InputIterator first, InputIterator last, OutputIterator out) {
                static_assert(
                    std::is_same<typename std::iterator_traits<InputIterator>::value_type, real_t>::value &&
                        std::is_same<typename std::iterator_traits<OutputIterator>::value_type, real_t>::value,
                    "Iterator does not math the value type. No implicit conversion is allowed");
                std::transform(first, last, out, std::ref(*this));
            }

            constexpr real_t Biquad::operator()(const real_t tick) noexcept {
                const auto out = b0_ * tick + w0_;
                w0_            = b1_ * tick - a1_ * out + w1_;
                w1_            = b2_ * tick - a2_ * out;
                return out;
            }

            constexpr Biquad::operator bool() const noexcept {
                return is_stable();
            }

            constexpr real_t Biquad::a0() const noexcept {
                return a0_;
            }

            constexpr real_t Biquad::a1() const noexcept {
                return a1_;
            }

            constexpr real_t Biquad::a2() const noexcept {
                return a2_;
            }

            constexpr real_t Biquad::b0() const noexcept {
                return b0_;
            }

            constexpr real_t Biquad::b1() const noexcept {
                return b1_;
            }

            constexpr real_t Biquad::b2() const noexcept {
                return b2_;
            }
        } // namespace filter
    }     // namespace dsp
} // namespace air

#endif // OPENAIR_BIQUAD_HPP
