/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: biquad_designer.hpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_BIQUAD_DESIGNER_HPP
#define OPENAIR_BIQUAD_DESIGNER_HPP

#include "math/constant.hpp"
#include "biquad.hpp"
#include <cmath>

namespace air {
    namespace dsp {

        namespace filter {
            enum class BiquadType {
                LowPass,
                HighPass,
                BandPassSkirtGain,
                BandPassPeakGain,
                Notch,
                AllPass,
                PeakingEQ,
                LowShelf,
                HighShelf
            };

            /**
            * \brief Computes the coefficients of a Biquad filter given the different parameter
            *
                Now, given:
        
                    sampleRate (the sampling frequency)
        
                    frequency ("wherever it's happenin', man."  "center" frequency
                        or "corner" (-3 dB) frequency, or shelf midpoint frequency,
                        depending on which filter type)
        
                    dBgain (used only for peaking and shelving filters)
                    bandwidth in octaves (between -3 dB frequencies for BPF and notch
                        or between midpoint (dBgain/2) gain frequencies for peaking EQ)
        
                     _or_ Q (the EE kind of definition)
        
                     _or_ S, a "shelf slope" parameter (for shelving EQ only).  when S = 1,
                        the shelf slope is as steep as it can be and remain monotonically
                        increasing or decreasing gain with frequency.  the shelf slope, in
                        dB/octave, remains proportional to S for all other values.
            * \cite https://music.columbia.edu/pipermail/music-dsp/2001-March/041752.html
            */

            template <BiquadType Type>
            inline air::dsp::Biquad BiquadDesigner(double fc, double sample_rate, double Q, double gain_db = 1) {
                const auto A       = std::sqrt(std::pow(10., gain_db / 20));
                const auto omega   = 2 * constants<double>::pi * fc / sample_rate;
                const auto omega_s = std::sin(omega);
                const auto omega_c = std::cos(omega);
                const auto alpha   = omega_s / (2 * Q);
                const auto beta    = std::sqrt(A) / Q;
                std::array<air::real_t, 3> a{}, b{};
                if (Type == BiquadType::LowPass) {
                    a[0] = static_cast<air::real_t>(1 + alpha);
                    a[1] = static_cast<air::real_t>(-2. * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha);
                    b[0] = static_cast<air::real_t>((1 - omega_c) / 2.);
                    b[1] = static_cast<air::real_t>(1 - omega_c);
                    b[2] = b[0];
                } else if (Type == BiquadType::HighPass) {
                    a[0] = static_cast<air::real_t>(1 + alpha);
                    a[1] = static_cast<air::real_t>(-2. * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha);
                    b[0] = static_cast<air::real_t>((1 + omega_c) / 2.);
                    b[1] = static_cast<air::real_t>(-(1 + omega_c));
                    b[2] = b[0];
                } else if (Type == BiquadType::BandPassSkirtGain) {
                    a[0] = static_cast<air::real_t>(1 + alpha);
                    a[1] = static_cast<air::real_t>(-2. * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha);
                    b[0] = static_cast<air::real_t>(Q * alpha);
                    b[1] = 0;
                    b[2] = static_cast<air::real_t>(-Q * alpha);
                } else if (Type == BiquadType::BandPassPeakGain) {
                    a[0] = static_cast<air::real_t>(1 + alpha);
                    a[1] = static_cast<air::real_t>(-2. * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha);
                    b[0] = (float) alpha;
                    b[1] = 0;
                    b[2] = static_cast<air::real_t>(-alpha);
                } else if (Type == BiquadType::Notch) {
                    a[0] = static_cast<air::real_t>(1 + alpha);
                    a[1] = static_cast<air::real_t>(-2 * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha);
                    b[0] = 1;
                    b[1] = a[1];
                    b[2] = 1;
                } else if (Type == BiquadType::AllPass) {
                    a[0] = static_cast<air::real_t>(1 + alpha);
                    a[1] = static_cast<air::real_t>(-2 * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha);
                    b[0] = a[2];
                    b[1] = a[1];
                    b[2] = a[0];
                } else if (Type == BiquadType::PeakingEQ) {
                    a[0] = static_cast<air::real_t>(1 + alpha / A);
                    a[1] = static_cast<air::real_t>(-2 * omega_c);
                    a[2] = static_cast<air::real_t>(1 - alpha / A);
                    b[0] = static_cast<air::real_t>(1 + alpha * A);
                    b[1] = a[1];
                    b[2] = static_cast<air::real_t>(1 - alpha * A);
                } else if (Type == BiquadType::LowShelf) {
                    a[0] = static_cast<air::real_t>((A + 1) + (A - 1) * omega_c + beta * omega_s);
                    a[1] = static_cast<air::real_t>(-2 * ((A - 1) + (A + 1) * omega_c));
                    a[2] = static_cast<air::real_t>((A + 1) + (A - 1) * omega_c - beta * omega_s);
                    b[0] = static_cast<air::real_t>(A * ((A + 1) - (A - 1) * omega_c + beta * omega_s));
                    b[1] = static_cast<air::real_t>(2 * A * ((A - 1) - (A + 1) * omega_c));
                    b[2] = static_cast<air::real_t>(A * ((A + 1) - (A - 1) * omega_c - beta * omega_s));
                } else if (Type == BiquadType::HighShelf) {
                    a[0] = static_cast<air::real_t>((A + 1) - (A - 1) * omega_c + beta * omega_s);
                    a[1] = static_cast<air::real_t>(2 * ((A - 1) - (A + 1) * omega_c));
                    a[2] = static_cast<air::real_t>((A + 1) - (A - 1) * omega_c - beta * omega_s);
                    b[0] = static_cast<air::real_t>(A * ((A + 1) + (A - 1) * omega_c + beta * omega_s));
                    b[1] = static_cast<air::real_t>(-2 * A * ((A - 1) + (A + 1) * omega_c));
                    b[2] = static_cast<air::real_t>(A * ((A + 1) + (A - 1) * omega_c - beta * omega_s));
                }
                return Biquad(a[0], a[1], a[2], b[0], b[1], b[2]);
            }
        } // namespace filter
    }     // namespace dsp
} // namespace air

#endif //OPENAIR_BIQUAD_DESIGNER_HPP
