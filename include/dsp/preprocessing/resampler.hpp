/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: resampler.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_RESAMPLER_HPP
#define OPENAIR_RESAMPLER_HPP

#include "core/core.hpp"
#include "meta/meta.hpp"
#include "base/base.hpp"
#include <samplerate.h>

namespace air {
    namespace dsp {

        class A_EXPORT ReSampler {
        public:
            enum class ConverterType { BestQuality = 0, MediumQuality, Fastest, ZeroOrderHold, Linear };
            A_EXPORT ReSampler(air::real_t input_sr, air::real_t output_sr, air::uint8_t channels,
                               ConverterType type = ConverterType::BestQuality);
            A_EXPORT ~ReSampler();

            template <typename InputIterator, typename OutputIterator>
            A_EXPORT inline void apply(InputIterator first, InputIterator last, OutputIterator first_out,
                                       OutputIterator last_out);

            template <typename Container>
            A_EXPORT inline void apply(const Container& input, Container& output);

            A_EXPORT air::uint8_t channels() const noexcept;
            A_EXPORT void set_channels(air::uint8_t channels) noexcept;

            A_EXPORT ConverterType type() const noexcept;
            A_EXPORT void set_type(ConverterType type) noexcept;

            A_EXPORT air::real_t input_samplerate() const noexcept;
            A_EXPORT void set_input_samplerate(air::real_t input_samplerate) noexcept;

            A_EXPORT air::real_t output_samplerate() const noexcept;
            A_EXPORT void set_output_samplerate(air::real_t output_samplerate) noexcept;

        private:
            void initialize();
            void configure(const air::real_t* input, air::size_t input_sz, air::real_t* output, air::size_t output_sz);
            void process();

        private:
            air::real_t input_samplerate_{8000};
            air::real_t output_samplerate_{8000};
            air::uint8_t channels_{1};
            ConverterType type_{ConverterType::BestQuality};
            SRC_STATE* state_ = nullptr;
            SRC_DATA data_;
        };

        template <typename InputIterator, typename OutputIterator>
        inline void ReSampler::apply(InputIterator first, InputIterator last, OutputIterator first_out,
                                     OutputIterator last_out) {
            const auto input_sz  = std::distance(first, last);
            const auto output_sz = std::distance(first_out, last_out);
            configure(&(*first), input_sz, &(*first_out), output_sz);
            process();
        }

        template <typename Container>
        void ReSampler::apply(const Container& input, Container& output) {
            const auto ratio = output_samplerate_ / input_samplerate_;
            output.resize(static_cast<std::size_t>(ratio * size(input) + 0.5));
            configure(data(input), size(input), data(output), size(output));
            process();
        }

    } // namespace dsp
} // namespace air

#endif //OPENAIR_RESAMPLER_HPP
