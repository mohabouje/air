/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: mixer.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_MIXER_HPP
#define OPENAIR_MIXER_HPP

#include <core/core.hpp>
#include <numeric>

namespace air {
    namespace dsp {

        template <typename InputIterator, typename OutputIterator>
        void mixer(InputIterator first, InputIterator last, air::int8_t channels, OutputIterator out) {
            for (; first < last; first += channels, out++) {
                *out = std::accumulate(first, first + channels, 0.) / static_cast<real_t>(channels);
            }
        };

        template <typename InputIterator, typename OutputIterator>
        void mixer(InputIterator first, InputIterator last, air::int8_t channels, air::int8_t desired_channel,
                   OutputIterator out) {
            for (; first < last; first += channels, out++) {
                *out = *(first + desired_channel);
            }
        };
    } // namespace dsp
} // namespace air

#endif //OPENAIR_MIXER_HPP
