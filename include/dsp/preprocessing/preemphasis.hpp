/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: pre_emphasis.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_PRE_EMPHASIS_HPP
#define OPENAIR_PRE_EMPHASIS_HPP

#include "dsp/filter/biquad.hpp"

namespace air {
    namespace dsp {

        class A_EXPORT PreEmphasis {
        public:
            A_EXPORT explicit PreEmphasis(real_t alpha);
            A_EXPORT real_t alpha() const noexcept;
            A_EXPORT void set_alpha(real_t alpha) noexcept;

            template <typename BiIterator>
            A_EXPORT void apply(BiIterator first, BiIterator last);

            template <typename InputIterator, typename OutputIterator>
            A_EXPORT void apply(InputIterator first, InputIterator last, OutputIterator out);

            A_EXPORT void reset() noexcept;

        private:
            filter::Biquad biquad_{1, 0, 0, 1, -0.95f, 0};
        };

        template <typename BiIterator>
        void PreEmphasis::apply(BiIterator first, BiIterator last) {
            biquad_.apply(first, last);
        }

        template <typename InputIterator, typename OutputIterator>
        void PreEmphasis::apply(InputIterator first, InputIterator last, OutputIterator out) {
            biquad_.apply(first, last, out);
        }

    } // namespace dsp
} // namespace air

#endif //OPENAIR_PRE_EMPHASIS_HPP
