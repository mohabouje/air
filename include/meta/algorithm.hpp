/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: algorithm.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_ALGORITHM_HPP
#define OPENAIR_ALGORITHM_HPP

#include <algorithm>
#include <iterator>
#include <type_traits>

namespace air {
    namespace meta {
        template <typename InputIterator>
        constexpr bool contains(InputIterator first, InputIterator last,
                                const typename std::iterator_traits<InputIterator>::value_type& value) {
            const auto iter = std::find(first, last, value);
            return (iter != last);
        }

        template <typename InputIterator, class InsertIterator, class ConditionalPredicate, class TransformPredicate>
        constexpr void transform_if(InputIterator first, InputIterator last, InsertIterator inserter,
                                    ConditionalPredicate predicate, TransformPredicate transformer) {
            for (; first != last; ++first) {
                if (predicate(*first)) {
                    auto copy = *first;
                    transformer(copy);
                    *inserter++ = copy;
                }
            }
        }
    } // namespace meta
} // namespace air

#endif // OPENAIR_ALGORITHM_HPP
