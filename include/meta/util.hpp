/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: util.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_UTIL_HPP
#define OPENAIR_UTIL_HPP

#include "core/types.hpp"

#include <cstring>
#include <iterator>
#include <limits>
#include <type_traits>

namespace air {
    inline namespace meta {
        template <typename T>
        constexpr air::boolean is_character_v = std::is_same<T, char>::value || std::is_same<T, wchar_t>::value;

        template <typename T>
        using is_character_t = typename std::enable_if<is_character_v<T>>::type;

        template <typename T>
        constexpr air::boolean is_signed_v =
            std::is_same<T, air::char_t>::value || std::is_same<T, air::int16_t>::value ||
            std::is_same<T, air::int32_t>::value || std::is_same<T, air::int64_t>::value;

        template <typename T>
        using is_signed_t = typename std::enable_if<is_signed_v<T>>::type;

        template <typename T>
        constexpr air::boolean is_unsigned_v =
            std::is_same<T, air::uint8_t>::value || std::is_same<T, air::uint16_t>::value ||
            std::is_same<T, air::uint32_t>::value || std::is_same<T, air::uint64_t>::value;

        template <typename T>
        using is_unsigned_t = typename std::enable_if<is_unsigned_v<T>>::type;

        template <typename T>
        using base_type = typename std::remove_cv<typename std::remove_reference<T>::type>::type;

        template <typename T>
        inline air::boolean isnull(const T* ptr) {
            return ptr != nullptr && ptr != NULL;
        };

        template <>
        inline air::boolean isnull<char>(const char* ptr) {
            return strlen(ptr) == 0;
        }

        template <typename T>
        inline air::boolean notnull(const T* ptr) {
            return !isnull(ptr);
        }

        template <typename T>
        constexpr air::boolean equal(T lhs, T rhs) {
            static_assert(!std::is_floating_point<T>::value, "Just floating point comparision");
            return lhs == rhs;
        };

        template <typename T, typename = typename std::enable_if<!std::is_trivially_copyable<T>::value>::type>
        constexpr air::boolean equal(const T& lhs, const T& rhs) {
            return lhs == rhs;
        };

        template <typename T, typename = typename std::enable_if<std::is_floating_point<T>::value>::type>
        constexpr air::boolean equal(T a, T b) {
            return std::abs(a - b) < std::numeric_limits<T>::epsilon();
        }

        template <typename T>
        constexpr air::boolean notequal(T lhs, T rhs) {
            return !equal(lhs, rhs);
        };

        template <typename T, typename = typename std::enable_if<!std::is_trivially_copyable<T>::value>::type>
        constexpr air::boolean notequal(const T& lhs, const T& rhs) {
            return !equal(lhs, rhs);
        };

#ifndef STANDARD_17

        template <typename T>
        constexpr air::boolean empty(const T& container) {
            return container.empty();
        }

        template <class T, std::size_t N>
        constexpr air::boolean empty(const T (&array)[N]) noexcept {
            return false;
        }

        template <class E>
        constexpr air::boolean empty(std::initializer_list<E> il) noexcept {
            return il.size() == 0;
        }

        template <typename T>
        constexpr air::boolean notempty(const T& element) {
            return !empty(element);
        }

        template <typename T>
        constexpr air::size_t size(const T& container) {
            return static_cast<air::size_t>(container.size());
        }

        template <class T, air::size_t N>
        constexpr air::size_t size(const T (&array)[N]) noexcept {
            return N;
        }

        template <class Container>
        constexpr const typename Container::value_type* data(const Container& container) {
            return container.data();
        }

        template <class Container>
        constexpr typename Container::value_type* data(Container& container) {
            return container.data();
        }

        template <typename T>
        constexpr const T* data(const std::basic_string<T>& str) {
            return str.c_str();
        }

        template <typename T>
        constexpr T* data(std::basic_string<T>& str) {
            return str.c_str();
        }

        template <class T, air::size_t N>
        constexpr const T* data(const T (&array)[N]) noexcept {
            return array;
        }

        template <class T, air::size_t N>
        constexpr T* data(T (&array)[N]) noexcept {
            return array;
        }

        template <class E>
        constexpr const E* data(std::initializer_list<E> il) noexcept {
            return il.begin();
        }

#endif
    } // namespace meta
} // namespace air

#endif // OPENAIR_UTIL_HPP
