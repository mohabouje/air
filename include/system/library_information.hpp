/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: library_information.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_LIBRARY_INFORMATION_HPP
#define OPENAIR_LIBRARY_INFORMATION_HPP

#include "core/core.hpp"
#include <string>

namespace air::system {
    class VersionNumber;
    class LibraryInformation {
    public:
        A_EXPORT static const char* build_date() noexcept;
        A_EXPORT static const char* build_time() noexcept;
        A_EXPORT static VersionNumber version() noexcept;
        A_EXPORT static bool is_debug() noexcept;
        A_EXPORT static bool is_release() noexcept;
    };
} // namespace air::system

#endif // OPENAIR_LIBRARY_INFORMATION_HPP
