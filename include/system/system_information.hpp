/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: system.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_SYSTEM_HPP
#define OPENAIR_SYSTEM_HPP

#include "core/core.hpp"

namespace air {
    namespace system {
        class SystemInformation {
        public:
            enum class OperativeSystemType { Windows, Linux, FreeBSD, MacOS, iOS, Android, Unknown };
            enum class ProcessorType { x86, x64, arm, Unknown };
            enum class CompilerType { gcc, clang, mvsc, Unknown };

            A_EXPORT static CompilerType compiler() noexcept;
            A_EXPORT static OperativeSystemType os() noexcept;
            A_EXPORT static ProcessorType processor() noexcept;
        };
    } // namespace system
} // namespace air

#endif // OPENAIR_SYSTEM_HPP
