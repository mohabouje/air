/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: version_number.hpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#ifndef OPENAIR_VERSION_NUMBER_HPP
#define OPENAIR_VERSION_NUMBER_HPP

#include "core/core.hpp"

namespace air {
    namespace system {
        class VersionNumber {
        public:
            A_EXPORT VersionNumber(air::uint16_t major, air::uint16_t minor, air::uint16_t micro) noexcept;
            A_EXPORT air::uint16_t major_version() noexcept;
            A_EXPORT air::uint16_t minor_version() noexcept;
            A_EXPORT air::uint16_t micro_version() noexcept;
            A_EXPORT air::string to_string();
            A_EXPORT static VersionNumber from_string(const std::string& version);

        private:
            air::uint16_t major_{0};
            air::uint16_t minor_{0};
            air::uint16_t micro_{0};
        };
    } // namespace system
} // namespace air

#endif // OPENAIR_VERSION_NUMBER_HPP
