/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: Logger.cpp
 * Created at: 08/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "base/logger.hpp"
using namespace air;

Logger::Logger(const air::string& type, Logger::MsgType msgType) :
    logger_(spdlog::get(type)),
    msgType_(msgType),
    msg_() {}

Logger::~Logger() {
    msg_ += '\n';
    /*switch (msgType_) {
        case MsgType::Debug:
            logger_->debug(msg_);
            break;
        case MsgType::Info:
            logger_->info(msg_);
            break;
        case MsgType::Warning:
            logger_->warn(msg_);
            break;
        case MsgType::Critical:
            logger_->critical(msg_);
            break;
        case MsgType::Error:
            logger_->error(msg_);
            break;
    }*/
}

Logger& Logger::operator<<(air::char_t character) {
    msg_ += character;
    return space();
}

Logger& Logger::operator<<(air::boolean state) {
    msg_ += (state ? "true" : "false");
    return space();
}

Logger& Logger::operator<<(const air::string& text) {
    msg_ += text;
    return space();
}

Logger& Logger::operator<<(air::int8_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::int16_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::int32_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::int64_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::uint8_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::uint32_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::uint16_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::uint64_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::float32_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(air::float64_t number) {
    msg_ += std::to_string(number);
    return space();
}

Logger& Logger::operator<<(const air::char_t* text) {
    msg_ += text;
    return space();
}

Logger& Logger::space() {
    msg_ += ' ';
    return *this;
}