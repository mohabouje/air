/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: dcremoval.cpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "dsp/preprocessing/dcremoval.hpp"

using namespace air;
using namespace air::dsp;

DCRemoval::DCRemoval(real_t alpha) : biquad_(1, -alpha, 0, 1 - alpha, alpha - 1, 0) {}

void DCRemoval::set_alpha(real_t alpha) noexcept {
    biquad_ = {1, -alpha, 0, 1 - alpha, alpha - 1, 0};
}

real_t DCRemoval::alpha() const noexcept {
    return biquad_.a1();
}

void DCRemoval::reset() noexcept {
    biquad_.reset();
}