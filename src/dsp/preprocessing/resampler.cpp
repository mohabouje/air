/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: resampler.cpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */
#include "dsp/preprocessing/resampler.hpp"

using namespace air;
using namespace air::dsp;

ReSampler::ReSampler(air::real_t input_sr, air::real_t output_sr, air::uint8_t channels,
                     ReSampler::ConverterType type) :
    input_samplerate_(input_sr),
    output_samplerate_(output_sr),
    channels_(channels),
    type_(type) {
    initialize();
}

ReSampler::~ReSampler() {
    if (state_ != nullptr) {
        src_delete(state_);
    }
}

air::uint8_t ReSampler::channels() const noexcept {
    return channels_;
}

void ReSampler::set_channels(air::uint8_t channels) noexcept {
    channels_ = channels;
    initialize();
}

ReSampler::ConverterType ReSampler::type() const noexcept {
    return type_;
}

void ReSampler::set_type(ReSampler::ConverterType type) noexcept {
    type_ = type;
    initialize();
}

air::real_t ReSampler::input_samplerate() const noexcept {
    return input_samplerate_;
}

void ReSampler::set_input_samplerate(air::real_t input_samplerate) noexcept {
    input_samplerate_ = input_samplerate;
}

air::real_t ReSampler::output_samplerate() const noexcept {
    return output_samplerate_;
}

void ReSampler::set_output_samplerate(air::real_t output_samplerate) noexcept {
    output_samplerate_ = output_samplerate;
}

void ReSampler::initialize() {
    if (state_ != nullptr) {
        src_delete(state_);
    }

    int error = 0;
    state_    = src_new(static_cast<std::underlying_type<ConverterType>::type>(type_), channels_, &error);
    if (isnull(state_)) {
        aError() << "Error while configuring libsamplerate. Error:" << src_strerror(error);
    }
}

void ReSampler::configure(const air::real_t* input, air::size_t input_sz, air::real_t* output, air::size_t output_sz) {
    const auto ratio       = output_samplerate_ / input_samplerate_;
    const auto expected_sz = static_cast<std::size_t>(ratio * input_sz + 0.5);

    if (expected_sz != output_sz) {
        aError() << "Error: output size does not match. Expected: " << output_sz;
    }

    data_.data_in       = input;
    data_.input_frames  = input_sz / channels_;
    data_.data_out      = output;
    data_.output_frames = output_sz / channels_;
    data_.src_ratio     = ratio;
}

void ReSampler::process() {
    if (auto error = src_process(state_, &data_) != 0) {
        aError() << "Error while executing libsamplerate. Error: " << src_strerror(error);
    }

    if (data_.output_frames_gen != data_.output_frames) {
        aWarning() << "Error while performing re-sampling process. Expected output with size " << data_.output_frames
                   << "but only" << data_.output_frames_gen << "samples has been generated";
    }
}