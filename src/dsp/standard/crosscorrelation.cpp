/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: CrossCorrelation.cpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "dsp/standard/crosscorrelation.hpp"

using namespace air;
using namespace air::dsp;

CrossCorrelation::CrossCorrelation(air::size_t sz, ScaleOpt opt) :
    fft_data_left_(std::vector<std::complex<real_t>>(std::floor(sz / 2) + 1)),
    fft_data_right_(std::vector<std::complex<real_t>>(std::floor(sz / 2) + 1)),
    size_(sz),
    scale_(opt) {}

air::size_t CrossCorrelation::size() const noexcept {
    return size_;
}

void CrossCorrelation::compute(const real_t* input_left, const real_t* input_right, real_t* output, air::size_t sz) {
    if (sz != size_) {
        aError() << "Error while computing the cross correlation. Expected buffer size: " << size_;
    }

    fft_.dft(fftw_cast(input_left), fftw_cast(meta::data(fft_data_left_)), sz);
    fft_.dft(fftw_cast(input_right), fftw_cast(meta::data(fft_data_right_)), sz);

    std::transform(std::cbegin(fft_data_left_), std::cend(fft_data_left_), std::cbegin(fft_data_right_),
                   std::begin(fft_data_right_), [](const std::complex<double> left, const std::complex<double> right) {
                       return left * std::conj(right);
                   });

    ifft_.idft(fftw_cast(meta::data(fft_data_right_)), fftw_cast(output), sz);

    // Scale the ifft & cross correlation scale option (Biased, Unbiased or None)
    const auto factor = sz * (scale_ == ScaleOpt::Biased ? sz : 1);
    std::transform(output, output + sz, output, [factor](real_t val) { return val / static_cast<real_t>(factor); });
}
