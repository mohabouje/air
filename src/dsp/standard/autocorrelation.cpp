/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: autocorrelation.cpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "dsp/standard/autocorrelation.hpp"

using namespace air;
using namespace air::dsp;

AutoCorrelation::AutoCorrelation(air::size_t sz, ScaleOpt opt) :
    fft_data_(std::vector<std::complex<real_t>>(std::floor(sz / 2) + 1)),
    size_(sz),
    scale_(opt) {}

air::size_t AutoCorrelation::size() const noexcept {
    return size_;
}

void AutoCorrelation::compute(const real_t* input, real_t* output, air::size_t sz) {
    if (sz != size_) {
        aError() << "Error while computing the auto correlation. Expected buffer size: " << size_;
    }

    fft_.dft(fftw_cast(input), fftw_cast(meta::data(fft_data_)), sz);

    std::transform(std::begin(fft_data_), std::end(fft_data_), std::begin(fft_data_),
                   [](std::complex<real_t> val) -> std::complex<real_t> {
                       const auto tmp = std::abs(val);
                       return {tmp * tmp, 0};
                   });

    ifft_.idft(fftw_cast(meta::data(fft_data_)), fftw_cast(output), sz);

    // Scale the ifft & auto correlation scale option (Biased, Unbiased or None)
    const auto factor = sz * (scale_ == ScaleOpt::Biased ? sz : 1);
    std::transform(output, output + sz, output, [factor](real_t val) { return val / static_cast<real_t>(factor); });
}