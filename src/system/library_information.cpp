/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: library_information.cpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "system/library_information.hpp"
#include "core/version.hpp"
#include "system/version_number.hpp"

using namespace air::system;

VersionNumber LibraryInformation::version() noexcept {
    return {A_VERSION_MAJOR, A_VERSION_MINOR, A_VERSION_MICRO};
}

const char* LibraryInformation::build_date() noexcept {
    return A_BUILD_DATE;
}

const char* LibraryInformation::build_time() noexcept {
    return A_BUILD_TIME;
}

bool LibraryInformation::is_debug() noexcept {
#ifdef A_DEBUG_BUILD
    return true;
#else
    return false;
#endif
}

bool LibraryInformation::is_release() noexcept {
#ifdef A_DEBUG_BUILD
    return false;
#else
    return true;
#endif
}