/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: version_number.cpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "system/version_number.hpp"
#include "core/config.hpp"

using namespace air::system;

VersionNumber::VersionNumber(air::uint16_t major, air::uint16_t minor, air::uint16_t micro) noexcept :
    major_(major),
    minor_(minor),
    micro_(micro) {}

VersionNumber VersionNumber::from_string(const std::string& version) {
    A_UNUSED(version);
    // TODO: parse string to version
    return {0, 0, 0};
}

std::string VersionNumber::to_string() {
    std::string tmp = std::to_string(major_);
    tmp += '.';
    tmp += std::to_string(minor_);
    tmp += '.';
    tmp += std::to_string(micro_);
    return tmp;
}

air::uint16_t VersionNumber::major_version() noexcept {
    return major_;
}

air::uint16_t VersionNumber::minor_version() noexcept {
    return minor_;
}

air::uint16_t VersionNumber::micro_version() noexcept {
    return micro_;
}