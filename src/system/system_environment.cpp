/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: system_environment.cpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "system/system_environment.hpp"
#include "base/logger.hpp"
#include "core/core.hpp"
#include "meta/util.hpp"
#include <mutex>

using namespace air::system;
using namespace air::meta;

static std::mutex environment_mutex;
std::string SystemEnvironment::get_env(const std::string& variable_name) {
    std::lock_guard<std::mutex> lock(environment_mutex);
    if (notempty(variable_name)) {
        const char* env_p = std::getenv(data(variable_name));
        if (notnull(env_p)) {
            return env_p;
        } else {
            aWarning() << "Variable" << variable_name << "not found";
        }
    } else {
        aWarning() << "Trying to access to an empty variable";
    }
    return std::string();
}

bool SystemEnvironment::set_env(const std::string& variable_name, const std::string& variable_value) {
    std::lock_guard<std::mutex> lock(environment_mutex);
    if (notempty(variable_name)) {
        return notequal(setenv(data(variable_name), data(variable_value), 1), -1);
    } else {
        aWarning() << "Trying to access to an empty variable";
    }
    return false;
}

bool SystemEnvironment::empty(const std::string& variable_name) {
    return empty(get_env(variable_name));
}

bool SystemEnvironment::exist(const std::string& variable_name) {
    std::lock_guard<std::mutex> lock(environment_mutex);
    if (notempty(variable_name)) {
        return notnull(std::getenv(data(variable_name)));
    } else {
        aWarning() << "Trying to access to an empty variable";
    }
    return false;
}