/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: system.cpp
 * Created at: 09/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "system/system_information.hpp"
#include "core/core.hpp"

using namespace air::system;
SystemInformation::OperativeSystemType SystemInformation::os() noexcept {
#if defined(A_OS_WINDOWS)
    return OperativeSystemType::Windows;
#elif defined(A_OS_LINUX)
    return OperativeSystemType::Linux;
#elif defined(A_OS_MACOS)
    return OperativeSystemType::MacOS;
#elif defined(A_OS_IOS)
    return OperativeSystemType::iOS;
#elif defined(A_OS_FREEBSD)
    return OperativeSystemType::FreeBSD;
#elif defined(A_OS_ANDROID)
    return OperativeSystemType::Android;
#else
    return OperativeSystemType::Unknown;
#endif
}

SystemInformation::ProcessorType SystemInformation::processor() noexcept {
#if defined(A_PROCESSOR_X86_32)
    return ProcessorType::x86;
#elif defined(A_PROCESSOR_X86_64)
    return ProcessorType::x64;
#elif defined(A_PROCESSOR_ARM)
    return ProcessorType::arm;
#else
    return ProcessorType::Unknown;
#endif
}

SystemInformation::CompilerType SystemInformation::compiler() noexcept {
#if defined(A_COMPILER_GNU)
    return CompilerType::gcc;
#elif defined(A_COMPILER_CLANG)
    return CompilerType::clang;
#elif defined(A_COMPILER_MVSC)
    return CompilerType::mvsc;
#else
    return CompilerType::Unknown;
#endif
}