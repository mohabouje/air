/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: testing_biquad.cpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "dsp/filter/biquad.hpp"
#include "meta/meta.hpp"
#include <gtest/gtest.h>

using namespace air;
using namespace air::dsp;

constexpr std::array<air::real_t, 14> hamming = {
    {0.0800000000000000, 0.1326902281995235, 0.2786902165036683, 0.4845531270825514, 0.7031182480395664,
     0.8843149441587066, 0.9866332360159840, 0.9866332360159840, 0.8843149441587066, 0.7031182480395666,
     0.4845531270825514, 0.2786902165036688, 0.1326902281995234, 0.0800000000000000}};

constexpr std::array<air::real_t, 14> filtered_hamming = {
    {0.0600000000000000, 0.1225176711496426, 0.2243161029851142, 0.4080561361272294, 0.6038674248104523,
     0.7454970053272414, 0.8343232053689902, 0.8383218172944669, 0.7436858710921658, 0.5892356489435457,
     0.4057487890439693, 0.2277438949872962, 0.1045839910529946, 0.0628555966544836}};

TEST(TestingBiquad, test_empty_input) {
    std::vector<air::real_t> input, output;
    filter::Biquad biquad;
    EXPECT_NO_THROW(biquad.apply(std::cbegin(input), std::cend(input), std::begin(output)));
}

TEST(TestingBiquad, test_default) {
    std::vector<air::real_t> output(air::size(hamming));
    filter::Biquad biquad;
    biquad.apply(std::cbegin(hamming), std::cend(hamming), std::begin(output));
    for (std::size_t i = 0; i < std::size(output); ++i) {
        EXPECT_NEAR(hamming[i], output[i], 0.00001);
    }
}

TEST(TestingBiquad, test_filtering_signal) {
    std::vector<air::real_t> output(air::size(hamming));
    filter::Biquad biquad(1, 0.75, 0.64, 0.75, 0.85, 0.41);
    biquad.apply(std::cbegin(hamming), std::cend(hamming), std::begin(output));
    for (std::size_t i = 0; i < std::size(output); ++i) {
        EXPECT_NEAR(output[i], filtered_hamming[i], 0.00001);
    }
}

TEST(TestingBiquad, test_biquad_pass) {
    std::vector<air::real_t> output(air::size(hamming));
    filter::Biquad biquad(1, 0, 0, 1, 0, 0);
    biquad.apply(std::cbegin(hamming), std::cend(hamming), std::begin(output));
    for (std::size_t i = 0; i < std::size(output); ++i) {
        EXPECT_NEAR(hamming[i], output[i], 0.00001);
    }
}
