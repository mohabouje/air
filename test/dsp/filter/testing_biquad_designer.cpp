/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: testing_biquad_designer.cpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#include <dsp/filter/biquad_designer.hpp>
#include <gtest/gtest.h>

using namespace air;
using namespace air::dsp::filter;

TEST(TestingBiquad, test_design_high_pass) {
    constexpr double fs                             = 24000;
    constexpr double fc                             = 100;
    constexpr double Q                              = 0.7;
    constexpr double G                              = 0;
    constexpr std::array<air::real_t, 3> expected_a = {{1, -1.96297974726857, 0.963652641985662}};
    constexpr std::array<air::real_t, 3> expected_b = {{0.981658097313558, -1.96331619462712, 0.981658097313558}};

    const auto biquad = BiquadDesigner<BiquadType::HighPass>(fc, fs, Q, G);

    EXPECT_NEAR(biquad.a0(), expected_a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), expected_a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), expected_a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), expected_b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), expected_b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), expected_b[2], 0.001);
}

TEST(TestingBiquad, test_design_band_pass_skirt_gain) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 0;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(0.0128506084259693);
    b[1] = 0;
    b[2] = static_cast<air::real_t>(-0.0128506084259693);
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.96297974726857);
    a[2] = static_cast<air::real_t>(0.963652641985662);

    const auto biquad = BiquadDesigner<BiquadType::BandPassSkirtGain>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}

TEST(TestingBiquad, test_design_band_pass_peak_gain) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 0;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(0.0181736790071691);
    b[1] = 0;
    b[2] = static_cast<air::real_t>(-0.0181736790071691);
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.96297974726857);
    a[2] = static_cast<air::real_t>(0.963652641985662);

    const auto biquad = BiquadDesigner<BiquadType::BandPassPeakGain>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}

TEST(TestingBiquad, test_design_notch) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 0;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(0.981826320992831);
    b[1] = static_cast<air::real_t>(-1.96297974726857);
    b[2] = static_cast<air::real_t>(0.981826320992831);
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.96297974726857);
    a[2] = static_cast<air::real_t>(0.963652641985662);

    const auto biquad = BiquadDesigner<BiquadType::Notch>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}

TEST(TestingBiquad, test_design_allpass) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 0;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(0.963652641985662);
    b[1] = static_cast<air::real_t>(-1.96297974726857);
    b[2] = 1;
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.96297974726857);
    a[2] = static_cast<air::real_t>(0.963652641985662);

    const auto biquad = BiquadDesigner<BiquadType::AllPass>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}

TEST(TestingBiquad, test_design_peaking_eq) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 14;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(1.03322966142232);
    b[1] = static_cast<air::real_t>(-1.98275466415748);
    b[2] = static_cast<air::real_t>(0.950204676145267);
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.98275466415748);
    a[2] = static_cast<air::real_t>(0.983434337567586);

    const auto biquad = BiquadDesigner<BiquadType::PeakingEQ>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}

TEST(TestingBiquad, test_design_low_shelf) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 14;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(1.0155934280269);
    b[1] = static_cast<air::real_t>(-1.97440406495045);
    b[2] = static_cast<air::real_t>(0.960326149525744);
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.9750106289924);
    a[2] = static_cast<air::real_t>(0.975313013510697);

    const auto biquad = BiquadDesigner<BiquadType::LowShelf>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}

TEST(TestingBiquad, test_design_high_shelf) {
    constexpr double fs = 24000;
    constexpr double fc = 100;
    constexpr double Q  = 0.7;
    constexpr double G  = 14;

    std::array<air::real_t, 3> a{};
    std::array<air::real_t, 3> b{};

    b[0] = static_cast<air::real_t>(4.9349200161819);
    b[1] = static_cast<air::real_t>(-9.74651948518657);
    b[2] = static_cast<air::real_t>(4.81309171241662);
    a[0] = 1;
    a[1] = static_cast<air::real_t>(-1.94408905223652);
    a[2] = static_cast<air::real_t>(0.945581295648469);

    const auto biquad = BiquadDesigner<BiquadType::HighShelf>(fc, fs, Q, G);
    EXPECT_NEAR(biquad.a0(), a[0], 0.001);
    EXPECT_NEAR(biquad.a1(), a[1], 0.001);
    EXPECT_NEAR(biquad.a2(), a[2], 0.001);
    EXPECT_NEAR(biquad.b0(), b[0], 0.001);
    EXPECT_NEAR(biquad.b1(), b[1], 0.001);
    EXPECT_NEAR(biquad.b2(), b[2], 0.001);
}