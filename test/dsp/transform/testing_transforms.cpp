/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: testing_transforms.cpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "dsp/transform/dct.hpp"
#include "dsp/transform/dft.hpp"
#include "meta/meta.hpp"
#include <gtest/gtest.h>

TEST(TestingFFT, complex_fft_and_ifft) {
    constexpr auto sz = 1024;
    std::vector<std::complex<air::real_t>> input(sz), data_fft(sz), data_ifft(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = std::complex<air::real_t>(i, i);
    }

    air::dsp::c_dft(input, data_fft);
    air::dsp::c_idft(data_fft, data_ifft);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(input[i].real(), data_ifft[i].real(), 0.01);
        EXPECT_NEAR(input[i].imag(), data_ifft[i].imag(), 0.01);
    }
}

TEST(TestingFFT, real_fft_and_ifft) {
    constexpr auto sz = 1024;
    std::vector<std::complex<air::real_t>> data_fft(sz);
    std::vector<air::real_t> input(sz), data_ifft(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = i;
    }

    air::dsp::dft(input, data_fft);
    air::dsp::idft(data_fft, data_ifft);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(input[i], data_ifft[i], 0.01);
        EXPECT_NEAR(input[i], data_ifft[i], 0.01);
    }
}

constexpr std::array<air::real_t, 64> hamming = {{
    0.0800, 0.0823, 0.0891, 0.1004, 0.1161, 0.1360, 0.1599, 0.1876, 0.2188, 0.2532, 0.2904, 0.3301, 0.3719,
    0.4154, 0.4601, 0.5056, 0.5515, 0.5972, 0.6424, 0.6865, 0.7292, 0.7700, 0.8085, 0.8444, 0.8772, 0.9067,
    0.9325, 0.9544, 0.9723, 0.9858, 0.9949, 0.9994, 0.9994, 0.9949, 0.9858, 0.9723, 0.9544, 0.9325, 0.9067,
    0.8772, 0.8444, 0.8085, 0.7700, 0.7292, 0.6865, 0.6424, 0.5972, 0.5515, 0.5056, 0.4601, 0.4154, 0.3719,
    0.3301, 0.2904, 0.2532, 0.2188, 0.1876, 0.1599, 0.1360, 0.1161, 0.1004, 0.0891, 0.0823, 0.0800,
}};

constexpr std::array<std::complex<air::real_t>, 64> hamming_fft = {
    {{34.1000, 0.0000}, {-14.8121, -0.7277}, {0.1589, 0.0157},  {0.0587, 0.0087},  {0.0309, 0.0061},  {0.0190, 0.0048},
     {0.0128, 0.0039},  {0.0091, 0.0033},    {0.0068, 0.0028},  {0.0052, 0.0024},  {0.0040, 0.0022},  {0.0032, 0.0019},
     {0.0026, 0.0017},  {0.0021, 0.0016},    {0.0017, 0.0014},  {0.0014, 0.0013},  {0.0011, 0.0011},  {0.0009, 0.0010},
     {0.0008, 0.0009},  {0.0006, 0.0009},    {0.0005, 0.0008},  {0.0004, 0.0007},  {0.0003, 0.0006},  {0.0003, 0.0005},
     {0.0002, 0.0005},  {0.0001, 0.0004},    {0.0001, 0.0003},  {0.0001, 0.0003},  {0.0000, 0.0002},  {0.0000, 0.0002},
     {0.0000, 0.0001},  {0.0000, 0.0001},    {0.0000, 0.0000},  {0.0000, -0.0001}, {0.0000, -0.0001}, {0.0000, -0.0002},
     {0.0000, -0.0002}, {0.0001, -0.0003},   {0.0001, -0.0003}, {0.0001, -0.0004}, {0.0002, -0.0005}, {0.0003, -0.0005},
     {0.0003, -0.0006}, {0.0004, -0.0007},   {0.0005, -0.0008}, {0.0006, -0.0009}, {0.0008, -0.0009}, {0.0009, -0.0010},
     {0.0011, -0.0011}, {0.0014, -0.0013},   {0.0017, -0.0014}, {0.0021, -0.0016}, {0.0026, -0.0017}, {0.0032, -0.0019},
     {0.0040, -0.0022}, {0.0052, -0.0024},   {0.0068, -0.0028}, {0.0091, -0.0033}, {0.0128, -0.0039}, {0.0190, -0.0048},
     {0.0309, -0.0061}, {0.0587, -0.0087},   {0.1589, -0.0157}, {-14.8121, 0.7277}}};

TEST(TestingFFT, complex_fft_hamming) {
    constexpr auto sz = air::size(hamming);
    std::vector<std::complex<air::real_t>> input(sz), data_fft(sz), data_ifft(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = std::complex<air::real_t>(hamming[i], 0);
    }

    air::dsp::c_dft(input, data_fft);
    air::dsp::c_idft(data_fft, data_ifft);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(data_fft[i].real(), hamming_fft[i].real(), 0.001);
        EXPECT_NEAR(data_fft[i].real(), hamming_fft[i].real(), 0.001);
        EXPECT_NEAR(input[i].real(), data_ifft[i].real(), 0.001);
        EXPECT_NEAR(input[i].imag(), data_ifft[i].imag(), 0.001);
    }
}

TEST(TestingDCT, complex_dct_and_idct_type_I) {
    constexpr auto sz = 1024;
    std::vector<air::real_t> input(sz), data_dct(sz), data_idct(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = i;
    }

    air::dsp::dct(input, data_dct, air::dsp::DCT_Type::Type_I);
    air::dsp::idct(data_dct, data_idct, air::dsp::DCT_Type::Type_I);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(input[i], data_idct[i], 0.01);
    }
}

TEST(TestingDCT, complex_dct_and_idct_type_II) {
    constexpr auto sz = 1024;
    std::vector<air::real_t> input(sz), data_dct(sz), data_idct(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = i;
    }

    air::dsp::dct(input, data_dct, air::dsp::DCT_Type::Type_II);
    air::dsp::idct(data_dct, data_idct, air::dsp::DCT_Type::Type_II);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(input[i], data_idct[i], 0.01);
    }
}

TEST(TestingDCT, complex_dct_and_idct_type_III) {
    constexpr auto sz = 1024;
    std::vector<air::real_t> input(sz), data_dct(sz), data_idct(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = i;
    }

    air::dsp::dct(input, data_dct, air::dsp::DCT_Type::Type_III);
    air::dsp::idct(data_dct, data_idct, air::dsp::DCT_Type::Type_III);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(input[i], data_idct[i], 0.01);
    }
}

TEST(TestingDCT, complex_dct_and_idct_type_IV) {
    constexpr auto sz = 1024;
    std::vector<air::real_t> input(sz), data_dct(sz), data_idct(sz);

    for (std::size_t i = 0; i < sz; ++i) {
        input[i] = i;
    }

    air::dsp::dct(input, data_dct, air::dsp::DCT_Type::Type_IV);
    air::dsp::idct(data_dct, data_idct, air::dsp::DCT_Type::Type_IV);
    for (std::size_t i = 0; i < sz; ++i) {
        EXPECT_NEAR(input[i], data_idct[i], 0.01);
    }
}
