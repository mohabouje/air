/*
 * OpenAIR, Audio Information Retrieval framework written in modern C++.
 * Copyright (C) 2018 Mohammed Boujemaoui Boulaghmoudi
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of  MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along withº
 * this program.  If not, see <http://www.gnu.org/licenses/>
 *
 * Filename: testing_autocorrelation.cpp
 * Created at: 10/06/18
 * Created by: Mohammed Boujemaoui
 */

#include "dsp/standard/autocorrelation.hpp"
#include <gtest/gtest.h>

using namespace air;
using namespace air::dsp;

TEST(TestingAutoCorrelation, test_even_input_without_normalization) {
    constexpr std::array<air::real_t, 24> hamming = {{0.080000, 0.097058, 0.146967, 0.226026, 0.328370, 0.446410,
                                                      0.571392, 0.694045, 0.805273, 0.896827, 0.961917, 0.995716,
                                                      0.995716, 0.961917, 0.896827, 0.805273, 0.694045, 0.571392,
                                                      0.446410, 0.328370, 0.226026, 0.146967, 0.097058, 0.080000}};

    constexpr std::array<air::real_t, 24> hamming_xcorr = {
        {9.146600, 9.049963, 8.776524, 8.339196, 7.760848, 7.071711, 6.306477, 5.501332,
         4.691202, 3.907391, 3.175791, 2.515752, 1.939628, 1.452974, 1.055286, 0.741148,
         0.501619, 0.325685, 0.201604, 0.118014, 0.064693, 0.032935, 0.015529, 0.006400}};

    constexpr auto sz = meta::size(hamming);
    std::vector<air::real_t> output(sz);

    AutoCorrelation xcorr(sz, AutoCorrelation::ScaleOpt::None);
    xcorr.compute(std::cbegin(hamming), std::cend(hamming), std::begin(output));
    for (air::size_t i = 0; i < sz; ++i) {
        ASSERT_NEAR(output[i], hamming_xcorr[i], 1);
    }
}
